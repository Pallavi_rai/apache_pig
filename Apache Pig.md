![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# Hands-on Lab Exercise 1-Apache Pig(A high-level Programming language)
**WHY we use Apache Pig ?**

- Did you know that 200 lines of MapReduce program can be written in just 10 lines of **pig** code.

- It analyzes bulk datasets.

- Similar to pigs, who eats anything, it can work upon any kind of data.

# Objective

After completing this lab,you will be able to know:

 -  How to install **Apache pig**

# Content
1. **JAVA Download**.
2. **HADOOP Download**.
3. **Apache Pig Download**.

# The Prerequisite for installing Apache pig are:

- Java 1.7

- Hadoop 2.X

# JAVA Download

**Note: This part of the exercise has been completed while doing Hadoop Lab, so if you have Java 1.7 and Hadoop installed in the system then you can skip both of these step and jump to Apache Pig Download.**

Check before downloading it because mostly this version is pre-installed in every system.

Go to command prompt and type command **java -version**.

if not installed then follow the below steps.

**Step 1:** Click on [Download java](https://www.oracle.com/java/technologies/javase/javase7-archive-downloads.html)

Select you operating system version and click on **jdk** link.

![java download](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_1.7_version__2_.png)


**Step 2:** After clicking on jdk link you will get this option shown below, just accept it and select download option.

![oracle signup](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_download_oracle_signin.png)

**Step 3:** After clicking on download option you will be directed to oracle login page. Create your account and sign in.

As you sign in, java jdk file will start downloading.

![signup](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_oracle.png)

**Step 4:** Click on downloaded **jdk** file and select “Next”

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_1.png)

**Step 5:** Click on **“Next”** and check it must be installed in C:\ drive only.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_2.png)

**Step 6:** Then Click **“Change”** and in C:\ drive, create new folder **“JAVA or similar name”** and click on “Next”.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_4.png)

**Step 7:** Java starts installing.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_5.png)

**Step 8:** Go to **“Program files"** in C:\ drive, you will find a folder named there as “java” open it and copy the jdk folder from there.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_6.png)

**Step 9:** Paste this jdk file in the folder created at installation time “JAVA’’ in C:\ drive. Open this JAVA folder and paste **jdk** file here.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_7.png)

**Step 10:** **NOTE:** Delete that java folder from program files in C:\ drive. There should be only one java file.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java.png)

### Environmental variables setting for JAVA.

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables.

Write variable name as: “JAVA_HOME” and variable value is the path location of jdk bin.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_8.png)

**Step 2:** How to get the path location of jdk bin.

Go to JAVA folder in C: drive --> open jdk1.7.0_80 file --> open bin. Then copy the path.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_9.png)

**Step 3:** Double Click on **path** in System Variables --> This opens path, click on **“New”** and then paste the path location of jdk bin here also. Then click **ok** on all open pages. Now you are done with setting up environment variables

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_10.png)


### Veryfying the installation of JAVA.(COMMAND PROMPT)

**Step 1:** Open Command prompt and type **“javac”** and enter.

![cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/cmd_java.png)

**Step 2:** To know the version of java installed type **“java -version”**. It will appear like this.

![cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/cmd_java_1.png)

# HADOOP Download

**Note: This part of the exercise has been completed while doing Hadoop Lab, so if you have Java 1.7 and Hadoop installed in the system then you can skip both of these step and jump to Apache Pig Download.**

**Step 1:** Click on [Download hadoop](https://hadoop.apache.org/release/2.4.1.html) 

Select **Download tar.gz** file(green box) and hadoop tar file will be downloaded.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.png)

**NOTE:** if there is no option like WinRAR then download it from this link [WinRAR](https://www.win-rar.com/predownload.html?&L=0) 

![winrar](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/winrar.png)

**Step 2:** Extract the downloaded file using WinRAR.

when you click on extract file this extraction path and options page will open.

Select C:\ drive and make new folder named as “HADOOP” then click ok.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_extract.png)

You can see, hadoop file is extracting.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/extracted_hadoop.png)

**After the extraction is completed, you will get this diagnostic message, just close it**

**Step 3:** Then you will see Hadoop folder created, open it and you will find a folder named **“hadoop-2.4.1”**. Open this folder all Hadoop files are inside this folder. 

**“NOTE: Within the Hadoop folder you will find another folder hadoop-2.4.1 containing all the hadoop files. Copy all the hadoop files from hadoop-2.4.1 folder and paste it inside the main folder Hadoop so that we have all files inside the main folder. After copying delete hadoop-2.4.1 folder. Now the bin path will be C:\Hadoop\bin.”**

### EDITION OF HADOOP etc FILE:

To edit hadoop file you will need **Notepad++**.

Click on this link or copy paste the link on new tab.

https://notepad-plus.en.softonic.com/download

![notepad](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/notepad.png)

**Lets start with editing now.**

**Step 1:** Open **Hadoop** folder --> open **“etc”** folder --> Inside you will find **hadoop** folder, open it:

We are going to edit 5 files here.

1st file= **core-site** (XML DOCUMENT) right click on this file and select option **edit with notepad++**.

Page will open like this:

**NOTE: don’t forget to save all the files after you paste your values in it. Save it after editing forsure.**

![core](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-1.png)

Copy and paste this value given below inside the configuration in core-site.xml file.

    
    <property>

        <name>fs.defaultFS</name>

        <value>hdfs://localhost:9000</value>

    </property>
`

After pasting it, it will look like this.

![core](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-4.png)

In the same way edit hdfs-site, mapred-site, and yarn-site (XML document).

**To assist you**

Value for each file is given below.

**Hdfs-site.xml**

Before editing this file, go to Hadoop folder in C:\ drive and create a new folder named as data.

Inside data folder create 2 folders **“namenode”** and **“datanode”**. And in value you must paste **path location** of your datanode and namenode.

![data](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-2.png)

**Values are:**

**Hdfs-site.xml**
   

    <property>

        <name>dfs.replication</name>

        <value>1</value>

    </property>

    <property>

        <name>dfs.namenode.name.dir</name>

        <value><path of namednode></value>

    </property>

    <property>

        <name>dfs.datanode.data.dir</name>

        <value><path of datanode></value>

    </property>



**Mapred-site.xml**

    <property>

        <name>mapreduce.framework.name</name>

        <value>yarn</value>

    </property>

**Yarn-site.xml**
    
    <property>

        <name>yarn.nodemanager.aux-services</name>

        <value>mapreduce_shuffle</value>

    </property>

    <property>

        <name>yarn.nodemanager.auxservices.mapreduce.shuffle.class</name>

        <value>org.apache.hadoop.mapred.ShuffleHandler</value>

    </property>

**Now open Hadoop-env (window command file) and edit it with notepad++.**

![hadoop cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_cmd.png)

Edit here **JAVA_HOME= “variable value"** you entered when you were creating the environmental variable for JAVA”. Open your setting and see your variable value. 

**Note: When you enter the Variable Value kindly remove the "bin".**

**REMINDER: do not forget to save your edited files.**

### SETTING UP THE ENVIRONMENT VARIABLES FOR HADOOP:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables section.

Write variable name as: **“HADOOP_HOME”** and variable value is the path location of bin.

![hadoophome](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_home.png)

**Step 2:** Then click on **“Path”** in System Variables, select **“New”** enter the variable value of Hadoop bin folder here as well.

![hadooppath](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_path.png)

**Step 3:** Then add the path location of hadoop’s **sbin** folder as well and click ok.

![sbin](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/sbin.png)

**In Hadoop we are missing some configuration file, let’s fix that**

**Step 1:** Click on this link https://drive.google.com/file/d/1AMqV4F5ybPF4ab4CeK8B3AsjdGtQCdvy/view or copy paste this link on new tab.download it.

**Step 2:** From download folder copy this file and paste it inside the Hadoop folder.

**Step 3:** Extract this Hadoop configuration file.

![configuration](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/configuration_file.png)

**Step 4:** A new folder named as **“HadoopConfiguration-FIXbin”** is created. Open it copy that bin folder and **replace** the previous bin. Now delete unnecessary files of Hadoop configuration.

### Veryfying the installation of HADOOP(COMMAND PROMPT)

**Step 1:** Open command prompt and type **“hdfs namenode -format”** and enter.

![hadoop.cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.cmd.png)

**Step 2:** Type **start-all.cmd** and enter.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.cmd_2.png)

**All files are opening now**

# APACHE PIG Download

**Step 1:** To download the Apache Pig, click on [Download Pig](https://downloads.apache.org/pig/)
 
Click on pig-0.17.0 and download pig-0.17.0.tar.gz file.

![pig](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/pig_1.png)

**Step 2:** Extract pig-0.17.0.tar.gz file using WinRAR and save the **pig folder** after extraction in **hadoop folder**.

**NOTE:SAVE THE EXTRACTED PIG FILE IN HADOOP FOLDER ONLY**

![pig](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/pig_extract_1.png)

**Step 3:** Pig is extracting.

![pig](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/pig_extract_2.png)

**Step 4:** pig-0.17.0 is created in hadoop folder.**RENAME** pig-0.17.0 as pig.

![PIG](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/PIG_3.png)

### SETTING UP THE ENVIRONMENT VARIABLES FOR APACHE PIG:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables.

Write variable name as: **“PIG_HOME”** and variable value is the path location of bin.

![PIG](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/PIG_4.png)

**Step 2:** Then click on **“Path”** in system variables, select **“New”** enter the variable value of Pig bin folder here as well.

![pig](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/PIG_5.png)

### EDITION OF PIG FILES:

**Step 1:** GO to **pig folder** inside the hadoop folder> open bin > open pig.cmd file and edit it with Notepad++

edit here this=**HADOOP_BIN_PATH=C:\hadoop\libexec**

![edit](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/edit.png)

**Step 2:** Go to C:\hadoop\libexec and open hadoop-config.cmd file and edit it with Notepad++

edit here this=**HADOOP_HOME=C:\hadoop\bin**

![e](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/pig_edit_2.png)

###### Veryfying the installation of APACHE PIG.(COMMAND PROMPT)

**Step 1:** Open cmd and type command **"pig -version"**

![pig](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/pig.cmd.png)

**Step 2:** To run pig type command **"pig or pig -x local"**

![pig](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/pig.cmd_2.png)

You can see **Grunt shell** started.

**APACHE PIG IS RUNNING**

**In the next lab we will run some commands on "Apache Pig".**

                                                                         **This Ends the Exercise.**














































